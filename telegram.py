from telethon import TelegramClient, events
import configparser
import asyncio
from parse import compile

from binance import make_order


def parse_query(s):
    reset = False
    if s == "/help":
        return {"help": None}

    p = compile("#{s1}/{s2}\nQuantity - {quantity}\n{is_long}")
    res = p.parse(s)
    if res is None:
        p = compile("#{s1}/{s2}")
        res = p.parse(s)

    if res is None:
        p = compile("reset #{s1}/{s2}")
        res = p.parse(s)
        reset = True

    if res:
        res = res.named
        res["symbol"] = f"{res.pop('s1')}{res.pop('s2')}"
        if "is_long" in res:
            if res["is_long"] != "Long" and res["is_long"] != "Short":
                res = None
            else:
                res["is_long"] = 1 if res["is_long"] == "Long" else 0

        if res and "quantity" in res:
            try:
                float(res["quantity"])
            except ValueError:
                res = None

    if reset:
        res["reset"] = True

    return res


def create_client():
    config = configparser.ConfigParser()
    config.read("config.ini")
    api_id = config["Telegram"]["api_id"]
    api_hash = config["Telegram"]["api_hash"]
    chat_name = config["Telegram"]["chat_name"]
    client = TelegramClient("session_log", api_id, api_hash)
    return client, chat_name


def main():
    client, chat_name = create_client()
    lock = asyncio.Lock()
    state_dict = dict()

    @client.on(events.NewMessage(chats=chat_name))
    async def message_handler(event):
        async with lock:
            try:
                nonlocal state_dict
                message = event.message.to_dict()["message"]
                args = parse_query(message)
                if not args:
                    await client.send_message(chat_name, "Cant parse query, check format by typing /help")
                    return
                elif "help" in args:
                    await client.send_message(chat_name, "Available commands:\n"
                                                         "[1] #coin1/coin2\nQuantity - val\nLong\\Short\n"
                                                         "[2] reset #coin1/coin2\n"
                                                         "[3] #coin1/coin2")
                elif "reset" in args and args["symbol"] in state_dict:
                    del state_dict[args["symbol"]]
                else:
                    symbol = args["symbol"]
                    if symbol not in state_dict:
                        state_dict[symbol] = {
                            "quantity": args["quantity"],
                            "is_long": args["is_long"],
                            "prev_quantity": None,
                            "short_volume": None,
                            "symbol": symbol}
                    try:
                        resp = make_order(**state_dict[symbol])
                    except Exception as e:
                        await client.send_message(chat_name, str(e))
                        return
                    if state_dict[symbol]["is_long"]:
                        state_dict[symbol]["prev_quantity"] = resp
                    else:
                        state_dict[symbol]["prev_quantity"], state_dict[symbol]["short_volume"] = resp

                    state_dict[symbol]["is_long"] ^= 1
            except Exception as e:
                print(e)
                exit(1)

    client.start()
    client.run_until_disconnected()


if __name__ == "__main__":
    main()
