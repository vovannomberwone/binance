import configparser
import hashlib
import hmac
import time
import urllib
from decimal import Decimal
from urllib.parse import urlparse
import requests


class Binance:
    methods = {
        # public methods
        "ping": {"url": "api/v1/ping", "method": "GET", "private": False},
        "time": {"url": "api/v1/time", "method": "GET", "private": False},
        "exchangeInfo": {"url": "api/v1/exchangeInfo", "method": "GET", "private": False},
        "depth": {"url": "api/v1/depth", "method": "GET", "private": False},
        "trades": {"url": "api/v1/trades", "method": "GET", "private": False},
        "historicalTrades": {"url": "api/v1/historicalTrades", "method": "GET", "private": False},
        "aggTrades": {"url": "api/v1/aggTrades", "method": "GET", "private": False},
        "klines": {"url": "api/v1/klines", "method": "GET", "private": False},
        "ticker24hr": {"url": "api/v1/ticker/24hr", "method": "GET", "private": False},
        "tickerPrice": {"url": "api/v3/ticker/price", "method": "GET", "private": False},
        "tickerBookTicker": {"url": "api/v3/ticker/bookTicker", "method": "GET", "private": False},
        "avgPrice": {"url": "api/v3/avgPrice", "method": "GET", "private": False},
        # private methods
        "createOrder": {"url": "api/v3/order", "method": "POST", "private": True},
        "testOrder": {"url": "api/v3/order/test", "method": "POST", "private": True},
        "orderInfo": {"url": "api/v3/order", "method": "GET", "private": True},
        "cancelOrder": {"url": "api/v3/order", "method": "DELETE", "private": True},
        "openOrders": {"url": "api/v3/openOrders", "method": "GET", "private": True},
        "allOrders": {"url": "api/v3/allOrders", "method": "GET", "private": True},
        "account": {"url": "api/v3/account", "method": "GET", "private": True},
        "myTrades": {"url": "api/v3/myTrades", "method": "GET", "private": True},
        # sapi
        "marginTransfer": {"url": "sapi/v1/margin/transfer", "method": "POST", "private": True},
        "marginLoan": {"url": "sapi/v1/margin/loan", "method": "POST", "private": True},
        "marginLoanGet": {"url": "sapi/v1/margin/loan", "method": "GET", "private": True},
        "marginRepay": {"url": "sapi/v1/margin/repay", "method": "POST", "private": True},
        "marginRepayGet": {"url": "sapi/v1/margin/repay", "method": "GET", "private": True},
        "marginCreateOrder": {"url": "sapi/v1/margin/order", "method": "POST", "private": True},
        "marginCancelOrder": {"url": "sapi/v1/margin/order", "method": "DELETE", "private": True},
        "marginOrderInfo": {"url": "sapi/v1/margin/order", "method": "GET", "private": True},
        "marginAccount": {"url": "sapi/v1/margin/account", "method": "GET", "private": True},
        "marginOpenOrders": {"url": "sapi/v1/margin/openOrders", "method": "GET", "private": True},
        "marginAllOrders": {"url": "sapi/v1/margin/allOrders", "method": "GET", "private": True},
        "marginAsset": {"url": "sapi/v1/margin/asset", "method": "GET", "private": True},
        "marginPair": {"url": "sapi/v1/margin/pair", "method": "GET", "private": True},
        "marginAllPairs": {"url": "sapi/v1/margin/allPairs", "method": "GET", "private": True},
        "marginPriceIndex": {"url": "sapi/v1/margin/priceIndex", "method": "GET", "private": True},
        "marginMyTrades": {"url": "sapi/v1/margin/myTrades", "method": "GET", "private": True},
        "marginMaxBorrowable": {"url": "sapi/v1/margin/maxBorrowable", "method": "GET", "private": True},
        "marginmaxTransferable": {"url": "sapi/v1/margin/maxTransferable", "method": "GET", "private": True}}

    def __init__(self, api_key, api_secret):
        self.api_key = api_key
        self.api_secret = bytearray(api_secret, encoding="utf-8")
        self.shift_seconds = 0

    def __getattr__(self, name):
        def wrapper(**kwargs):
            kwargs.update(command=name)
            return self.call_api(**kwargs)

        return wrapper

    def call_api(self, **kwargs):
        method = self.methods[kwargs.pop("command")]
        api_url = f"https://api.binance.com/{method['url']}"
        payload = kwargs
        headers = {}
        payload_str = urllib.parse.urlencode(payload)

        if method["private"]:
            payload.update({"timestamp": int(time.time() + self.shift_seconds) * 1000})
            payload_str = urllib.parse.urlencode(payload).encode("utf-8")
            sign = hmac.new(key=self.api_secret, msg=payload_str, digestmod=hashlib.sha256).hexdigest()
            payload_str = f"{payload_str.decode('utf-8')}&signature={sign}"
            headers = {"X-MBX-APIKEY": self.api_key, "Content-Type": "application/json"}

        api_url += "?" + payload_str if method["method"] == "GET" or method["url"].startswith("sapi") else ""
        response = requests.request(method=method["method"], url=api_url,
                                    data="" if method["method"] == "GET" else payload_str, headers=headers)

        if response.status_code != 200:
            raise Exception(response.text)
        return response.json()


def create_bot():
    config = configparser.ConfigParser()
    config.read("config.ini")
    api_key = config["Binance"]["api_key"]
    api_secret = config["Binance"]["api_secret"]
    bot = Binance(api_key, api_secret)
    commission_coef = config["Binance"]["commission_coef"]
    return bot, commission_coef


def nearest_step(minim, step_size, x):
    return minim if x < minim else x - (x - minim) % step_size


def get_quote_info(bot, symbol_t):
    info = bot.exchangeInfo()
    for symbol in info["symbols"]:
        if symbol["symbol"] == symbol_t:
            precision = Decimal(f"1.{symbol['quotePrecision'] * '0'}")
            for filt in symbol["filters"]:
                if filt["filterType"] == "LOT_SIZE":
                    minQty = Decimal(filt["minQty"])
                    stepSize = Decimal(filt["stepSize"])
                # if filt["filterType"] == "MIN_NOTIONAL":
                #     print(filt["minNotional"], filt["avgPriceMins"])
            break
    else:
        raise Exception(f"cant find symbol: {symbol_t}")

    return minQty, stepSize, precision


def make_order(symbol, is_long, short_volume, prev_quantity=None, quantity=None):
    bot, commission_coef = create_bot()
    minQty, stepSize, precision = get_quote_info(bot, symbol)

    if prev_quantity is None:
        quantity = nearest_step(minQty, stepSize, Decimal(quantity)).quantize(precision)
        if is_long:
            bot.marginCreateOrder(symbol=symbol, side="BUY", type="MARKET", quantity=quantity, recvWindow=7000,
                                  sideEffectType="MARGIN_BUY")
            return quantity
        else:
            resp = bot.marginCreateOrder(symbol=symbol, side="SELL", type="MARKET", quantity=quantity, recvWindow=7000,
                                         sideEffectType="MARGIN_BUY")
            return quantity, resp["cummulativeQuoteQty"]

    if is_long:
        quantity = Decimal(prev_quantity).quantize(precision)
        resp = bot.marginCreateOrder(symbol=symbol, side="BUY", type="MARKET", quantity=quantity, recvWindow=7000,
                                     sideEffectType="AUTO_REPAY")
        cur_price = Decimal(bot.marginPriceIndex(symbol=symbol)["price"])
        quantity = (2 * Decimal(short_volume) - Decimal(resp["cummulativeQuoteQty"])) / cur_price
        quantity = nearest_step(minQty, stepSize, quantity).quantize(precision)
        bot.marginCreateOrder(symbol=symbol, side="BUY", type="MARKET", quantity=quantity, recvWindow=7000,
                              sideEffectType="MARGIN_BUY")
        return quantity
    else:
        quantity = nearest_step(minQty, stepSize, Decimal(commission_coef) * Decimal(prev_quantity).quantize(precision))
        resp = bot.marginCreateOrder(symbol=symbol, side="SELL", type="MARKET", quantity=quantity, recvWindow=7000,
                                     sideEffectType="AUTO_REPAY")
        cur_price = Decimal(bot.marginPriceIndex(symbol=symbol)["price"])
        quantity = nearest_step(minQty, stepSize, Decimal(resp["cummulativeQuoteQty"]) / cur_price).quantize(precision)
        resp = bot.marginCreateOrder(symbol=symbol, side="SELL", type="MARKET", quantity=quantity, recvWindow=7000,
                                     sideEffectType="MARGIN_BUY")
        return quantity, resp["cummulativeQuoteQty"]
